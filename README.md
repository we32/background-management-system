# Vue 3 + TypeScript + Vite

```
使用vue3+TypeScript+vite+pinia+elementuiPlug+高德地图等完成后台管理系统，所有数据全部通过本地模拟生成
ps:本人对TypeScript比较生疏，所以有很多地方都是用的any或者vscode的ts报错我也没有管。
```

### 主要技术

```
1、vue3
2、vue-router4
3、pinia
4、elementuiPlug
5、高德地图
6、vite
```

### 运行

```
1、npm i
2、npm run dev
```

#### 目前还存在的问题（实时更新）

```
1、路由警告
Component "default" in record with path "/home" is a function that does not return a Promise. If you were passing a functional component, make sure to add a "displayName" to the component. This will break in production if not fixed.
```

![](D:\自己项目\background-management-system\src\assets\problem\路由警告.png)
