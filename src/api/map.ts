import { markerLocal } from '../json/map'

export const getMarkerLocal = () =>{
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(markerLocal)
    }, 200)
  })
}