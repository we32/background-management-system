type MarkerLocal = {
  lng:string,
  lat:string,
  name:string,
  content:string
  extData:ExtData
}
type ExtData = {
  id:number,
  address:string,
  lng:string,
  lat:string
}

export const markerLocal = [
  {
    lng: '104.141094',
    lat: '30.628931',
    name: '测试数据1',
    content: '70',
    extData: {
      id: 1,
      address: '测试数据111',
      lng: '104.141094',
      lat: '30.628931',
    },
  },
  {
    lng: '104.141010',
    lat: '30.628106',
    name: '测试数据2',
    content: '50',
    extData: {
      id: 2,
      address: '测试数据222',
      lng: '104.141010',
      lat: '30.628106',
    },
  },
]