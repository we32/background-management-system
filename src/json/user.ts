type Rsp = {
  code:number,
  message:string,
  menu:Array<any>,
  token:string
}
const userMenuAndRouters = [
  {
    name:'顶级',
    path:'/',
    redirect: '/home',
    component:'Layout/index',
    children:[
      {
        name: '数据大屏',
        path: '/home',
        icon:'FullScreen',
        component: 'Home/index',
      },
      {
        name: '地图监控',
        path: '/map',
        icon:'MapLocation',
        component: 'Map/index',
      },
      {
        name: '项目管理',
        path:'/project',
        icon:'HelpFilled',
        children:[
          {
            name: '台账管理',
            path: 'money',
            component: 'Project/money',
          },
          {
            name: '执行管理',
            path: 'implement',
            component: 'Project/implement',
          },
          {
            name: '画像分析',
            path: 'portrait',
            component: 'Project/portrait',
          },
          {
            name: '广告搭建',
            path: 'advertisement',
            component: 'Project/advertisement',
          },
        ]
      },
      {
        name: '用户管理',
        path: '/user',
        icon:'UserFilled',
        component: 'User/index',
      },
      {
        name: '权限管理',
        path: '/jurisdiction',
        icon:'Suitcase',
        component: 'Jurisdiction/index',
      },
      {
        name: '系统管理',
        path: '/system',
        icon:'Grid',
        component: 'System/index',
      }
    ]
  }
]
export const rspUserLogin:Rsp = {
  code:200,
  message:'登陆成功',
  menu:userMenuAndRouters,
  token:'asndasidjaois12313'
}